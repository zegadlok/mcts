﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS
{
    public static class Constants
    {
        public const int numberOfRows = 6;
        public const int numberOfColumns = 7;
        public const int fieldSize = 50;
        public const double c = 1.41;
    }
}
