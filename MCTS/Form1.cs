﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MCTS
{
    public partial class Form1 : Form
    {
        private GameState gameState;
        private BufferedGraphicsContext currentContext = BufferedGraphicsManager.Current;
        private bool playerStarts;
        private bool playerMoves;
        public Form1()
        {
            InitializeComponent();
            gameState = new GameState();
            pictureBox1.Width = Constants.numberOfColumns * Constants.fieldSize;
            pictureBox1.Height = Constants.numberOfRows * Constants.fieldSize;
            numericUpDown1.Maximum = Constants.numberOfColumns;
            playerStarts = true;
            playerMoves = true;

            //gameState.Move(3, StoneColor.white);
            //gameState.Move(3, StoneColor.black);
            //gameState.Move(3, StoneColor.white);
            //gameState.Move(3, StoneColor.white);
            //gameState.Move(3, StoneColor.black);
            //gameState.Move(3, StoneColor.white);

            //gameState.Move(2, StoneColor.white);
            //gameState.Move(2, StoneColor.black);
            //gameState.Move(2, StoneColor.black);
            //gameState.Move(2, StoneColor.white);
            //gameState.Move(2, StoneColor.white);

            //gameState.Move(1, StoneColor.black);
            //gameState.Move(1, StoneColor.black);
            //gameState.Move(1, StoneColor.black);
            //gameState.Move(1, StoneColor.white);

            //gameState.Move(0, StoneColor.white);
            //gameState.Move(0, StoneColor.white);
            //gameState.Move(0, StoneColor.white);

            gameState.Move(4, StoneColor.black);
            gameState.Move(4, StoneColor.black);
            gameState.Move(4, StoneColor.black);
            gameState.Move(4, StoneColor.white);

            gameState.Move(3, StoneColor.black);
            gameState.Move(3, StoneColor.white);
            gameState.Move(3, StoneColor.white);

            gameState.Move(2, StoneColor.black);
            gameState.Move(2, StoneColor.white);

            gameState.Move(1, StoneColor.white);

            bool res = gameState.HasGameEnded(out StoneColor sc);
        }

        private void VisualizeGameState()
        {
            Graphics g = pictureBox1.CreateGraphics();
            BufferedGraphics buffer = currentContext.Allocate(g, pictureBox1.DisplayRectangle);
            buffer.Graphics.Clear(pictureBox1.BackColor);

            Pen p = new Pen(Color.Black);
            SolidBrush sb = new SolidBrush(Color.Black);

            for (int i = 1; i <= Constants.numberOfColumns - 1; i++)
            {
                int x = i * Constants.fieldSize;
                buffer.Graphics.DrawLine(p, x, 0, x, pictureBox1.Height - 1);
            }

            for (int i = 1; i <= Constants.numberOfRows - 1; i++)
            {
                int y = i * Constants.fieldSize;
                buffer.Graphics.DrawLine(p, 0, y, pictureBox1.Width-1, y);
            }

            for(int i=0; i< Constants.numberOfRows;i++)
            {
                for(int j=0;j<Constants.numberOfColumns;j++)
                {
                    int drawY = Constants.numberOfRows - i - 1;
                    if(gameState.GetSquare(i,j) == StoneColor.white)
                    {                      
                        buffer.Graphics.DrawEllipse(p, (j + 0.1f) * Constants.fieldSize,
                            (drawY + 0.1f) * Constants.fieldSize, 0.8f * Constants.fieldSize, 0.8f * Constants.fieldSize);
                    }
                    else if (gameState.GetSquare(i, j) == StoneColor.black)
                    {
                        buffer.Graphics.FillEllipse(sb, (j + 0.1f) * Constants.fieldSize, 
                            (drawY + 0.1f) * Constants.fieldSize, 0.8f * Constants.fieldSize, 0.8f * Constants.fieldSize);
                    }
                }
            }
            buffer.Render();
        }

        private void Button1_Click(object sender, EventArgs e)
        {           
            StoneColor playerSC = playerStarts == true ? StoneColor.white : StoneColor.black;
            StoneColor botSC = playerStarts == true ? StoneColor.black : StoneColor.white;
            int column = (int)numericUpDown1.Value - 1;
            StoneColor winner;

            if (gameState.HasGameEnded(out winner) == true)
            {
                WriteResult(winner);

                return;
            }

            while (playerMoves == true)
            {
                bool result = gameState.Move(column, playerSC);
                if (result == true)
                {
                    label1.Text = "Bot moves";
                    label1.Refresh();
                    playerMoves = !playerMoves;
                    VisualizeGameState();
                    break;
                }
            }
            if(gameState.HasGameEnded(out winner) == false)
            {
                MCTSNode root = new MCTSNode(gameState, playerSC);
                root.Initialize();
                gameState.Move(root.ChooseMove(botSC), botSC);
                playerMoves = !playerMoves;
                VisualizeGameState();
                label1.Text = "Human moves";
            }
            else
            {
                WriteResult(winner);
            }
        }

        private void WriteResult(StoneColor winner)
        {
            if (winner == StoneColor.none) label1.Text = "Draw!";
            else label1.Text = winner.ToString() + " won!";
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            DialogResult reuslt = MessageBox.Show("Should the human player start?", "Who goes first?", MessageBoxButtons.YesNoCancel);
            if (reuslt == DialogResult.Yes)
            {
                playerStarts = true;
                label1.Text = "Human moves";
                label2.Text = "White: human";
                label3.Text = "Black: bot";
                gameState = new GameState();
                VisualizeGameState();
            }
            else if(reuslt == DialogResult.No)
            {
                playerStarts = false;
                label1.Text = "Bot moves";
                label2.Text = "White: bot";
                label3.Text = "Black: human";
                gameState = new GameState();
                MCTSNode root = new MCTSNode(gameState, StoneColor.black);
                root.Initialize();
                gameState.Move(root.ChooseMove(StoneColor.white), StoneColor.white);
                playerMoves = true;
                VisualizeGameState();
            }
        }
    }
}
