﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS
{
    public enum StoneColor
    {
        none,
        white,
        black
    }
    public class GameState
    {
        private StoneColor[,] state;
        private int stonesDropped;

        public GameState()
        {
            state = new StoneColor[Constants.numberOfRows, Constants.numberOfColumns];
            stonesDropped = 0;
        }

        public GameState(GameState original)
        {
            state = new StoneColor[Constants.numberOfRows, Constants.numberOfColumns];
            stonesDropped = original.stonesDropped;
            for(int i=0; i< Constants.numberOfRows;i++)
            {
                for(int j=0; j< Constants.numberOfColumns;j++)
                {
                    state[i, j] = original.state[i, j];
                }
            }
        }
        
        public StoneColor GetSquare(int row, int column)
        {
            return state[row, column];
        }

        public bool Move(int column, StoneColor sc)
        {
            if (state[Constants.numberOfRows - 1, column] != StoneColor.none) return false;
            int i = 0;
            while (state[i, column] != StoneColor.none) i++;
            state[i, column] = sc;
            ++stonesDropped;
            return true;
        }

        public bool CanMoveBeDone(int column)
        {
            return state[Constants.numberOfRows - 1, column] == StoneColor.none;
        }

        public bool HasGameEnded(out StoneColor winner)  //zwraca czy gra sie zakonczyla + kolor zwyciezcy
        {
            for(int i = Constants.numberOfRows - 1; i >= 0; i--) 
            {
                int count = 0;
                StoneColor currentColor = StoneColor.none;
                for(int j = 0; j < Constants.numberOfColumns; j++) // sprawdzanie wierszy
                {
                    if(currentColor == StoneColor.none)
                    {
                        if(state[i, j] == StoneColor.white) ++count;                        
                        else if(state[i, j] == StoneColor.black) ++count;                        
                    }
                    else if(currentColor == StoneColor.white)
                    {
                        if (state[i, j] == StoneColor.none) count = 0;
                        else if (state[i, j] == StoneColor.white) ++count;                        
                        else count = 1;
                    }
                    else
                    {
                        if (state[i, j] == StoneColor.none) count = 0;
                        else if (state[i, j] == StoneColor.white) count = 1;                        
                        else ++count;                       
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    if (Constants.numberOfColumns - j + count < 5) break;
                    currentColor = state[i, j];
                }
            }

            for (int i = Constants.numberOfColumns - 1; i >= 0; i--)
            {
                int count = 0;
                StoneColor currentColor = StoneColor.none;
                for (int j = 0; j < Constants.numberOfRows; j++) // sprawdzanie column
                {
                    if (currentColor == StoneColor.none)
                    {
                        if (state[j,i] == StoneColor.white) ++count;
                        else if (state[j, i] == StoneColor.black) ++count;
                    }
                    else if (currentColor == StoneColor.white)
                    {
                        if (state[j, i] == StoneColor.none) count = 0;
                        else if (state[j, i] == StoneColor.white) ++count;
                        else count = 1;
                    }
                    else
                    {
                        if (state[j, i] == StoneColor.none) count = 0;
                        else if (state[j, i] == StoneColor.white) count = 1;
                        else ++count;
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    if (Constants.numberOfRows - j + count < 5) break;
                    currentColor = state[j,i];
                }
            }

            for(int diagonal = 3; diagonal <= Constants.numberOfRows - 1; diagonal++) 
            {
                int count = 0;
                StoneColor currentColor = StoneColor.none;
                //diagonale od szczecina do rzeszowa zaczynajace sie od lewej krawedzi
                for (int i=0; i <= diagonal;i++) 
                {
                    int row = diagonal - i;

                    if (i >= Constants.numberOfColumns) break;
                    if (row >= Constants.numberOfRows) break;

                    if (currentColor == StoneColor.none)
                    {
                        if (state[row, i] == StoneColor.white) ++count;
                        else if (state[row, i] == StoneColor.black) ++count;
                    }
                    else if (currentColor == StoneColor.white)
                    {
                        if (state[row, i] == StoneColor.none) count = 0;
                        else if (state[row, i] == StoneColor.white) ++count;
                        else count = 1;
                    }
                    else
                    {
                        if (state[row, i] == StoneColor.none) count = 0;
                        else if (state[row, i] == StoneColor.white) count = 1;
                        else ++count;
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    if (row + count < 4) break;
                    currentColor = state[row, i];
                }
            }

            for (int diagonal = Constants.numberOfRows; 
                diagonal <= Constants.numberOfColumns + Constants.numberOfRows - 5; diagonal++)
            {
                int count = 0;
                StoneColor currentColor = StoneColor.none;
                //diagonale od szczecina do rzeszowa, reszta
                for (int i = 0; i <= diagonal; i++)
                {
                    int row = Constants.numberOfRows - 1 - i;
                    int column = diagonal - row;

                    if (column >= Constants.numberOfColumns) break;
                    if (row >= Constants.numberOfRows) break;

                    if (currentColor == StoneColor.none)
                    {
                        if (state[row, column] == StoneColor.white) ++count;
                        else if (state[row, column] == StoneColor.black) ++count;
                    }
                    else if (currentColor == StoneColor.white)
                    {
                        if (state[row, column] == StoneColor.none) count = 0;
                        else if (state[row, column] == StoneColor.white) ++count;
                        else count = 1;
                    }
                    else
                    {
                        if (state[row, column] == StoneColor.none) count = 0;
                        else if (state[row, column] == StoneColor.white) count = 1;
                        else ++count;
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    //if (diagonal - i + count < 4) break;                    
                    currentColor = state[row,column];
                }
            }

            for(int diagonal = Constants.numberOfColumns - 4; diagonal >= 0; diagonal--)
            {
                //diagonale od suwałk do wrocławia od lewej krawędzi
                int count = 0;
                StoneColor currentColor = StoneColor.none;

                for (int i = 0; i < Constants.numberOfColumns; i++)
                {
                    int row = diagonal + i;
                    if (row >= Constants.numberOfRows) break;

                    if (currentColor == StoneColor.none)
                    {
                        if (state[row, i] == StoneColor.white) ++count;
                        else if (state[row, i] == StoneColor.black) ++count;
                    }
                    else if (currentColor == StoneColor.white)
                    {
                        if (state[row, i] == StoneColor.none) count = 0;
                        else if (state[row, i] == StoneColor.white) ++count;
                        else count = 1;
                    }
                    else
                    {
                        if (state[row, i] == StoneColor.none) count = 0;
                        else if (state[row, i] == StoneColor.white) count = 1;
                        else ++count;
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    currentColor = state[row, i];
                }
            }

            for(int diagonal = -1; diagonal >= 4 - Constants.numberOfColumns; diagonal--)
            {
                //diagonale od suwałk do wrocławia, reszta
                int count = 0;
                StoneColor currentColor = StoneColor.none;
                for (int i = 0; i < Constants.numberOfRows; i++)
                {
                    int column = i - diagonal;
                    if (column >= Constants.numberOfColumns) break;

                    if (currentColor == StoneColor.none)
                    {
                        if (state[i,column] == StoneColor.white) ++count;
                        else if (state[i, column] == StoneColor.black) ++count;
                    }
                    else if (currentColor == StoneColor.white)
                    {
                        if (state[i, column] == StoneColor.none) count = 0;
                        else if (state[i, column] == StoneColor.white) ++count;
                        else count = 1;
                    }
                    else
                    {
                        if (state[i, column] == StoneColor.none) count = 0;
                        else if (state[i, column] == StoneColor.white) count = 1;
                        else ++count;
                    }
                    if (count == 4)
                    {
                        winner = currentColor;
                        return true;
                    }
                    currentColor = state[i, column];
                }
            }
            winner = StoneColor.none;
            if (stonesDropped == Constants.numberOfColumns * Constants.numberOfRows) return true;           
            return false;
        }
    }
}
