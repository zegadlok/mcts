﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MCTS
{
    public class MCTSNode
    {
        public MCTSNode[] children;
        public MCTSNode parent;

        public static int allVisitsCount = 0;
        public int nodeVisitsCount;

        public double points; // wygrana - 1, remis - 0,5, przegrana - 0

        GameState gameState;
        StoneColor currentColor;

        public MCTSNode(GameState gameState, StoneColor cc, MCTSNode parent = null)
        {
            children = new MCTSNode[Constants.numberOfColumns];
            this.parent = parent;
            nodeVisitsCount = 0;
            points = 0;
            this.gameState = gameState;
            currentColor = cc;
        }

        public void Initialize()
        {
            for(int i=0; i<children.Length;i++)
            {
                StoneColor cc = currentColor == StoneColor.white ? StoneColor.black : StoneColor.white;

                children[i] = new MCTSNode(null, cc, this);
                children[i].nodeVisitsCount = 0;
                children[i].points = 0;
                allVisitsCount = 0;
            }
        }

        public GameState ChooseChild(out MCTSNode lastNode) 
        {
            MCTSNode pointer = this;
            double best = double.NegativeInfinity;
            GameState gs = new GameState(gameState);
            int index;
            bool end = false;
            bool terminal = false;

            while(true)
            {
                index = 0;
                best = double.NegativeInfinity;
                if (pointer.children == null) pointer.children = new MCTSNode[Constants.numberOfColumns];
                if (gs.HasGameEnded(out StoneColor placeholder) == true)
                {
                    lastNode = pointer;
                    return gs;
                }
                for (int i=0;i<pointer.children.Length;i++) // wynik dwęzła: nieskończoność
                {
                    if (gs.CanMoveBeDone(i) == false) continue; 
                    if(pointer.children[i] == null)
                    {
                        index = i;                       
                        if (currentColor == StoneColor.black) currentColor = StoneColor.white;
                        else currentColor = StoneColor.black;
                        gs.Move(i, currentColor);
                        end = true;
                        break;
                    }
                    else
                    {
                        if(pointer.children[i].nodeVisitsCount == 0) // wynik węzła: nieskończoność
                        {
                            index = i;
                            //simulationState.Move(i,currentColor);
                            //end = true;
                            break;
                        }
                        else
                        {
                            double exploitation = pointer.children[i].points / pointer.children[i].nodeVisitsCount;
                            double exploration = Constants.c * Math.Sqrt(Math.Log(allVisitsCount) / pointer.children[i].nodeVisitsCount);
                            double v = exploitation + exploration;
                            if(v > best)
                            {
                                best = v;
                                index = i;
                            }
                        }
                    }
                }
                if (end == true) break;
                pointer = pointer.children[index];              
                if (currentColor == StoneColor.black) currentColor = StoneColor.white;
                else currentColor = StoneColor.black;
                gs.Move(index, currentColor);
            }
            StoneColor cc = pointer.currentColor == StoneColor.white ? StoneColor.black : StoneColor.white;
            pointer.children[index] = new  MCTSNode(null, cc, pointer);
            lastNode = pointer.children[index];
            return gs;
        }

        public StoneColor Simulation(GameState choice)
        {
            StoneColor result;
            //int i = 0;
            while(choice.HasGameEnded(out result) == false)
            {
                //if (i == 0)
                //{
                //    start.children[index] = new MCTSNode(null, StoneColor.none, start);
                //    choice.Move(index, currentColor);
                //}
                //else
                //{
                    Random rand = new Random(Guid.NewGuid().GetHashCode());
                    int move;
                    while (true)
                    {
                        move = rand.Next() % Constants.numberOfColumns;
                        if (choice.CanMoveBeDone(move) == true) break;
                    }                   
                //}
               // ++i;
                currentColor = currentColor == StoneColor.black ? StoneColor.white : StoneColor.black;
                choice.Move(move, currentColor);
            }
            return result;
        }

        public int ChooseMove(StoneColor botSC)
        {
            int it = 100000;
            MCTSNode m = this;
            while(allVisitsCount < 100000)
            {
                StoneColor currentColor = botSC == StoneColor.black ? StoneColor.white : StoneColor.black;
                GameState choice;
                int index;
                MCTSNode ch;
                choice = ChooseChild(out ch);
                StoneColor w = Simulation(choice);

                if (w == StoneColor.none)
                {
                    while (ch != null) // propagacja wstecz
                    {
                        ch.points += 0.5;
                        ++ch.nodeVisitsCount;
                        ch = ch.parent;
                    }
                }
                else
                {
                    while (ch != null)
                    {
                        if (w == ch.currentColor) ch.points += 1;
                        ++ch.nodeVisitsCount;
                        ch = ch.parent;
                    }
                }         
                ++allVisitsCount;
                m = this;
            }
            int max = int.MinValue;
            int move=-1;
            for(int i=0; i< children.Length;i++)
            {
                if (children[i] == null) continue;
                if(children[i].nodeVisitsCount > max)
                {
                    max = children[i].nodeVisitsCount;
                    move = i;
                }
            }
            return move;
        }
    }
}
